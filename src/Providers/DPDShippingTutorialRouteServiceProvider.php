<?php
namespace DPDShippingTutorial\Providers;

use Plenty\Plugin\RouteServiceProvider;
use Plenty\Plugin\Routing\Router;

/**
 * Class DPDShippingTutorialRouteServiceProvider
 * @package DPDShippingTutorial\Providers
 */
class DPDShippingTutorialRouteServiceProvider extends RouteServiceProvider
{
    /**
     * @param Router $router
     */
    public function map(Router $router)
    {
        $router->post('shipment/plenty_tutorial/register_shipments', [
            'middleware' => 'oauth',
            'uses'       => 'DPDShippingTutorial\Controllers\ShipmentController@registerShipments'
        ]);
  	}

}
