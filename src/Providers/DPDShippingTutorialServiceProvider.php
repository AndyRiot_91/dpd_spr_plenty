<?php
namespace DPDShippingTutorial\Providers;

use Plenty\Modules\Order\Shipping\ServiceProvider\Services\ShippingServiceProviderService;
use Plenty\Plugin\ServiceProvider;

/**
 * Class DPDShippingTutorialServiceProvider
 * @package DPDShippingTutorial\Providers
 */
class DPDShippingTutorialServiceProvider extends ServiceProvider
{

	/** Register the service provider */
	public function register()
	{
	    // add REST routes by registering a RouteServiceProvider if necessary
//	     $this->getApplication()->register(DPDShippingTutorialRouteServiceProvider::class);
    }

    public function boot(ShippingServiceProviderService $shippingServiceProviderService)
    {

        $shippingServiceProviderService->registerShippingProvider(
            'DPDShippingTutorial',
            [
                'de' => 'SpringGDS Tracked',
                'en' => 'SpringGDS Tracked',
            ],
            [
                'DPDShippingTutorial\\Controllers\\ShippingController@registerShipments',
                'DPDShippingTutorial\\Controllers\\ShippingController@getLabels',
                'DPDShippingTutorial\\Controllers\\ShippingController@deleteShipments',
            ]);
    }
}
