<?php

namespace DPDShippingTutorial\Controllers;

use Plenty\Modules\Account\Address\Contracts\AddressRepositoryContract;
use Plenty\Modules\Account\Address\Models\Address;
use Plenty\Modules\Cloud\Storage\Models\StorageObject;
use Plenty\Modules\Item\Variation\Models\Variation;
use Plenty\Modules\Item\VariationSku\Models\VariationSku;
use Plenty\Modules\Order\Contracts\OrderRepositoryContract;
use Plenty\Modules\Order\Shipping\Contracts\ParcelServicePresetRepositoryContract;
use Plenty\Modules\Order\Shipping\Information\Contracts\ShippingInformationRepositoryContract;
use Plenty\Modules\Order\Shipping\Package\Contracts\OrderShippingPackageRepositoryContract;
use Plenty\Modules\Order\Shipping\PackageType\Contracts\ShippingPackageTypeRepositoryContract;
use Plenty\Modules\Order\Shipping\ParcelService\Models\ParcelServicePreset;
use Plenty\Modules\Plugin\Storage\Contracts\StorageRepositoryContract;
use Plenty\Plugin\Controller;
use Plenty\Plugin\Http\Request;
use Plenty\Plugin\ConfigRepository;
use Plenty\Modules\Order\Shipping\Package\Models\OrderShippingPackage;

use Plenty\Modules\Item\Item\Models\Item;
use Plenty\Modules\Item\Item\Contracts\ItemRepositoryContract;
use Plenty\Modules\Item\Variation\Contracts\VariationRepositoryContract;
use Plenty\Modules\Item\VariationSku\Contracts\VariationSkuRepositoryContract;

/**
 * Class ShippingController
 */
class ShippingController extends Controller
{
    /** @var array $services */
    private $services = array(
        'TRCK' => 'DPDShippingTutorial TRACKED',
        'UNTR' => 'DPDShippingTutorial UNTRACKED',
        'PPBNT' => 'DPDShippingTutorial Packet Boxable Non Tracked',
        'PPTR' => 'DPDShippingTutorial Packet Registered',
        'PPTT' => 'DPDShippingTutorial Packet Tracked',
        'PPBTT' => 'DPDShippingTutorial Packet Boxable Tracked',
    );

    /** @var Request $request */
    private $request;

    /** @var OrderRepositoryContract $orderRepository */
    private $orderRepository;

    /** @var AddressRepositoryContract $addressRepository */
    private $addressRepository;

    /** @var OrderShippingPackageRepositoryContract $orderShippingPackage */
    private $orderShippingPackage;

    /** @var ShippingInformationRepositoryContract $shippingInformationRepositoryContract */
    private $shippingInformationRepositoryContract;

    /** @var StorageRepositoryContract $storageRepository */
    private $storageRepository;

    /** @var ShippingPackageTypeRepositoryContract $shippingPackageTypeRepositoryContract */
    private $shippingPackageTypeRepositoryContract;

    /** @var  array $createOrderResult */
    private $createOrderResult = [];

    /** @var ConfigRepository $config */
    private $config;

    /** @var ItemRepositoryContract $irc */
    private $irc;

    /** @var VariationRepositoryContract $vrc */
    private $vrc;

    /** @var VariationSkuRepositoryContract $sku */
    private $sku;

    /**
     * ShippingController constructor.
     * @param Request $request
     * @param OrderRepositoryContract $orderRepository
     * @param AddressRepositoryContract $addressRepositoryContract
     * @param OrderShippingPackageRepositoryContract $orderShippingPackage
     * @param StorageRepositoryContract $storageRepository
     * @param ShippingInformationRepositoryContract $shippingInformationRepositoryContract
     * @param ShippingPackageTypeRepositoryContract $shippingPackageTypeRepositoryContract
     * @param ConfigRepository $config
     * @param ItemRepositoryContract $irc
     * @param VariationRepositoryContract $vrc
     * @param VariationSkuRepositoryContract $sku
     */
    public function __construct(Request $request,
                                OrderRepositoryContract $orderRepository,
                                AddressRepositoryContract $addressRepositoryContract,
                                OrderShippingPackageRepositoryContract $orderShippingPackage,
                                StorageRepositoryContract $storageRepository,
                                ShippingInformationRepositoryContract $shippingInformationRepositoryContract,
                                ShippingPackageTypeRepositoryContract $shippingPackageTypeRepositoryContract,
                                ConfigRepository $config,
                                ItemRepositoryContract $irc,
                                VariationRepositoryContract $vrc,
                                VariationSkuRepositoryContract $sku)
    {
        $this->request = $request;
        $this->orderRepository = $orderRepository;
        $this->addressRepository = $addressRepositoryContract;
        $this->orderShippingPackage = $orderShippingPackage;
        $this->storageRepository = $storageRepository;

        $this->shippingInformationRepositoryContract = $shippingInformationRepositoryContract;
        $this->shippingPackageTypeRepositoryContract = $shippingPackageTypeRepositoryContract;

        $this->config = $config;
        $this->irc = $irc;
        $this->vrc = $vrc;
        $this->sku = $sku;
    }

    /**
     * Registers shipment(s)
     * @param Request $request
     * @param array $orderIds
     * @param string $service
     * @return array
     */
    public function registerShipments(Request $request, $orderIds = array(), $service = 'TRCK')
    {
        try {
            $items = array();
            $orderIds = $this->getOpenOrderIds($this->getOrderIds($request, $orderIds));
            $shipmentDate = date('Y-m-d');

            foreach ($orderIds as $orderId) {
                $order = $this->orderRepository->findOrderById($orderId);
                /** @var Address $address */
                $address = $order->deliveryAddress;
                // gets order shipping packages from current order
                $packages = $this->orderShippingPackage->listOrderShippingPackages($order->id);

                foreach ($packages as $package) {
                    $packageType = $this->shippingPackageTypeRepositoryContract->findShippingPackageTypeById($package->packageId);

                    try {
                        foreach ($order->orderItems as $item) {
                            if ($item->itemVariationId) {
                                $varById = (array)$this->vrc->show($item->itemVariationId, array(), 'en');
                                $itemById = $this->irc->show($varById['itemId'], array(), 'en'); // urlPath

                                $this->debug(
                                    array(
                                        '$itemById' => $itemById,
                                        '(array)$itemById' => ((array)$itemById),
                                    )
                                );

                                /** dynamic property names are not allowed in plenty */
                                switch ($this->config->get('DPDShippingTutorial.itemTitle', 'name1')) {
                                    case 'name1':
                                        $description = $itemById->texts[0]->name1;
                                        break;
                                    case 'name2':
                                        $description = $itemById->texts[0]->name2;
                                        break;
                                    case 'name3':
                                        $description = $itemById->texts[0]->name3;
                                        break;
                                    case 'shortDescription':
                                        $description = $itemById->texts[0]->shortDescription;
                                        break;
                                    default:
                                        $description = $item->orderItemName;
                                }

                                $items[] = array(
                                    'Description' => $description,
                                    'Sku' => $varById->model ?? $varById['model'],
                                    'HsCode' => (!empty($itemById->customsTariffNumber)
                                        ? $itemById->customsTariffNumber
                                        : $this->config->get('DPDShippingTutorial.hsCode')
                                    ),
                                    'OriginCountry' => '',
                                    'PurchaseUrl' => '',//'https://domain.xyz/' . $itemById->urlPath,
                                    'Quantity' => $item->quantity,
                                    'Value' => '',
                                );
                            }
                        }

                        $shipment = json_encode(
                            array(
                                'Apikey' => $this->config->get('DPDShippingTutorial.apiKey'),
                                "Command" => 'OrderShipment',
                                'Shipment' => array(
                                    "LabelFormat" => $this->config->get('DPDShippingTutorial.format', 'PDF'),
                                    "ShipperReference" => $order->id . "-" . $order->plentyId, // unique
                                    "Service" => $service,
                                    "ConsignorAddress" => array(
                                        "Name" => $this->config->get('DPDShippingTutorial.senderName'),
                                        "Company" => $this->config->get('DPDShippingTutorial.companyName'),
                                        "AddressLine1" => $this->config->get('DPDShippingTutorial.senderStreet') . ' ' . $this->config->get('DPDShippingTutorial.senderNo'),
                                        "AddressLine2" => "",
                                        "City" => $this->config->get('DPDShippingTutorial.senderTown'),
                                        "State" => $this->config->get('DPDShippingTutorial.senderState'),
                                        "Zip" => $this->config->get('DPDShippingTutorial.senderPostalCode'),
                                        "Country" => $this->config->get('DPDShippingTutorial.senderCountryIso'), // iso
                                        "Phone" => $this->config->get('DPDShippingTutorial.senderPhone'),
                                        "Email" => $this->config->get('DPDShippingTutorial.senderEmail'),
                                    ),
                                    "ConsigneeAddress" => array(
                                        "Name" => $address->firstName . " " . $address->lastName,
                                        "Company" => $address->companyName,
                                        "AddressLine1" => $address->address1,
                                        "AddressLine2" => $address->address2,
                                        "City" => $address->town,
                                        "State" => $address->state->isoCode ?: '',
                                        "Zip" => $address->postalCode,
                                        "Country" => $address->country->isoCode2,
                                        "Phone" => (!empty(trim($address->phone)) ? $address->phone : '0000000000'),
                                        "Email" => $address->email,
                                        "Vat" => '',
                                    ),
                                    "Length" => $packageType->length,
                                    "Width" => $packageType->width,
                                    "Weight" => $package->weight / 1000, // todo
                                    "Height" => $packageType->height,
                                    "WeightUnit" => "kg", // todo
                                    "DimUnit" => "cm", // todo
                                    "Value" => $order->amounts[0]->invoiceTotal,
                                    "Currency" => $order->amounts[0]->currency,
                                    "DeclarationType" => $this->config->get('DPDShippingTutorial.declarationType'),
                                    "Products" => $items,
                                    'Source' => 'plentymarkets',
                                )
                            )
                        );

                        $responseApi = $this->curlApi($shipment);

                        // shipping service providers API should be used here
                        $response = [
                            'labelUrl' => $this->genLabelUrl($responseApi->Shipment->TrackingNumber),
                            'shipmentNumber' => $responseApi->Shipment->TrackingNumber,
                            'sequenceNumber' => 1,
                            'status' => 'shipment successfully registered as ' . $this->services[$service],
                        ];

                        $shipmentItems = $this->handleAfterRegisterShipment(
                            $response['labelUrl'],
                            $response['shipmentNumber'],
                            $package->id
                        );

                        // adds result
                        $this->createOrderResult[$orderId] = [
                            'success' => (empty($responseApi->Error) && $responseApi->ErrorLevel == 0),
                            'message' => (empty($responseApi->Error) && $responseApi->ErrorLevel == 0
                                ? 'Shipment successfully registered'
                                : 'Code: ' . $responseApi->ErrorLevel . ': ' . $responseApi->Error),
                            'newPackagenumber' => false,
                            'packages' => $shipmentItems,
                        ];

                        // saves shipping information
                        $this->saveShippingInformation($orderId, $shipmentDate, $shipmentItems);
                    } catch (\SoapFault $soapFault) {
                        // handle exception
                    }
                }
            }
        } catch (\Exception $e) {
            $this->debug(array('Exception' => $e->getMessage()));
        }

        return $this->createOrderResult;
    }

    /**
     * @param Request $request
     * @param array $orderIds
     * @return array
     */
    public function getLabels(Request $request, $orderIds = array()): array
    {
        $orderIds = $this->getOrderIds($request, $orderIds);
        $labels = array();

        foreach ($orderIds as $orderId) {
            /** @var OrderShippingPackage $packages */
            $packages = $this->orderShippingPackage->listOrderShippingPackages($orderId);

            foreach ($packages as $package) {
                $labelKey = array_pop(explode('/', $package->labelPath));

                if ($this->storageRepository->doesObjectExist('DPDShippingTutorial', $labelKey)) {
                    $storageObject = $this->storageRepository->getObject('DPDShippingTutorial', $labelKey);

                    $labels[] = $storageObject->body;
                }
            }
        }

        return $labels;
    }

    /**
     * Cancels registered shipment(s)
     *
     * @param Request $request
     * @param array $orderIds
     * @return array
     */
    public function deleteShipments(Request $request, $orderIds)
    {
        $orderIds = $this->getOrderIds($request, $orderIds);

        foreach ($orderIds as $orderId) {
            $shippingInformation = $this->shippingInformationRepositoryContract->getShippingInformationByOrderId($orderId);
            try {
                // use the shipping service provider's API here
                $response = $this->curlApi(
                    json_encode(
                        array(
                            'Apikey' => $this->config->get('DPDShippingTutorial.apiKey'),
                            'Command' => 'VoidShipment',
                            'Shipment' => array(
                                'TrackingNumber' => $shippingInformation->transactionId,
//                                'ShipperReference' => $shippingInformation->orderId . "-" . $order->plentyId,
                            ),
                        )
                    )
                );

                $this->createOrderResult[$orderId] = [
                    'success' => (empty($response->Error) && $response->ErrorLevel == 0),
                    'message' => (empty($response->Error) && $response->ErrorLevel == 0
                        ? 'Shipment canceled'
                        : 'Code: ' . $response->ErrorLevel . ': ' . $response->Error),
                    'newPackagenumber' => false,
                    'packages' => null,
                ];
            } catch (\SoapFault $soapFault) {
                // exception handling
            }

            if (isset($shippingInformation->additionalData) && is_array($shippingInformation->additionalData)) {
                // resets the shipping information of current order
                $this->shippingInformationRepositoryContract->resetShippingInformation($orderId);
            }
        }

        // return result array
        return $this->createOrderResult;
    }

    /**
     * Handling of response values, fires S3 storage and updates order shipping package
     *
     * @param string $labelUrl
     * @param string $shipmentNumber
     * @param string $sequenceNumber
     * @return array
     */
    private function handleAfterRegisterShipment($labelUrl, $shipmentNumber, $sequenceNumber)
    {
        $shipmentItems = array();
        $storageObject = $this->saveLabelToS3(
            $labelUrl,
            $shipmentNumber . '.pdf'
        );

        $shipmentItems[] = array(
            'labelUrl' => $labelUrl,
            'shipmentNumber' => $shipmentNumber,
        );

        $this->orderShippingPackage->updateOrderShippingPackage(
            $sequenceNumber,
            array(
                'packageNumber' => $shipmentNumber,
                'label' => $storageObject->key
            )
        );

        return $shipmentItems;
    }

    /**
     * Retrieves the label file from a given URL and saves it in S3 storage
     *
     * @param $labelUrl
     * @param $key
     * @return StorageObject
     */
    private function saveLabelToS3($labelUrl, $key)
    {
        $ch = curl_init();
        // Set URL to download
        curl_setopt($ch, CURLOPT_URL, $labelUrl);
        // Include header in result? (0 = yes, 1 = no)
        curl_setopt($ch, CURLOPT_HEADER, 0);
        // Should cURL return or print out the data? (true = return, false = print)
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Timeout in seconds
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        // Download the given URL, and return output
        $output = curl_exec($ch);
        // Close the cURL resource, and free system resources
        curl_close($ch);

        return $this->storageRepository->uploadObject('DPDShippingTutorial', $key, $output);
    }

    /**
     * Saves the shipping information
     *
     * @param $orderId
     * @param $shipmentDate
     * @param $shipmentItems
     */
    private function saveShippingInformation($orderId, $shipmentDate, $shipmentItems)
    {
        $transactionIds = array();
        foreach ($shipmentItems as $shipmentItem) {
            $transactionIds[] = $shipmentItem['shipmentNumber'];

        }

        $shipmentAt = date(\DateTime::W3C, strtotime($shipmentDate));
        $registrationAt = date(\DateTime::W3C);

        $data = [
            'orderId' => $orderId,
            'transactionId' => implode(',', $transactionIds),
            'shippingServiceProvider' => 'DPDShippingTutorial',
            'shippingStatus' => 'registered',
            'shippingCosts' => 0.00,
            'additionalData' => $shipmentItems,
            'registrationAt' => $registrationAt,
            'shipmentAt' => $shipmentAt

        ];
        $this->shippingInformationRepositoryContract->saveShippingInformation(
            $data);
    }

    /**
     * Returns all order ids with shipping status 'open'
     *
     * @param array $orderIds
     * @return array
     */
    private function getOpenOrderIds($orderIds)
    {
        $openOrderIds = array();

        foreach ($orderIds as $orderId) {
            $shippingInformation = $this->shippingInformationRepositoryContract->getShippingInformationByOrderId($orderId);

            if ($shippingInformation->shippingStatus == null || $shippingInformation->shippingStatus == 'open') {
                $openOrderIds[] = $orderId;
            }
        }

        return $openOrderIds;
    }

    /**
     * Returns all order ids from request object
     *
     * @param Request $request
     * @param $orderIds
     * @return array
     */
    private function getOrderIds(Request $request, $orderIds)
    {
        if (is_numeric($orderIds)) {
            $orderIds = array($orderIds);
        } elseif (!is_array($orderIds)) {
            $orderIds = $request->get('orderIds');
        }

        return $orderIds;
    }

    /**
     * @param string $data
     * @return object
     */
    public function curlApi(string $data): object
    {
        $ch = curl_init(
            'https://mtapi.net/'
            . ($this->config->get('DPDShippingTutorial.mode', '0') === '0' ? '?testMode=1' : '')
        );
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_TIMEOUT, 100);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $response = json_decode(curl_exec($ch));
        curl_close($ch);

        return $response;
    }

    /**
     * @param string $trackingNumber
     * @return string
     */
    public function genLabelUrl(string $trackingNumber = ''): string
    {
        return 'https://mailingtechnology.com/API/label_plenty.php?testMode='
            . (int)!$this->config->get('DPDShippingTutorial.mode', '0')
            . '&userId=api&tn=' . $trackingNumber
            . '&label_format=' . $this->config->get('DPDShippingTutorial.format', 'PDF')
            . '&apikey=' . $this->config->get('DPDShippingTutorial.apiKey', '');
    }

    /**
     * @param array $data
     * @return void
     */
    public static function debug(array $data = array()): void
    {
        $ch = curl_init('https://acpapi.com/dima/plenty/index.php');
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            http_build_query(array('some-paranoia' => 'no', 'data' =>
                json_encode($data))));
        curl_setopt($ch, CURLOPT_TIMEOUT, 100);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($ch);
        curl_close($ch);
    }

}
