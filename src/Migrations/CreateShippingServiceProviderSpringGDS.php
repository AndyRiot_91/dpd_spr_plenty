<?php

namespace DPDShippingTutorial\Migrations;

use Plenty\Modules\Order\Shipping\ServiceProvider\Contracts\ShippingServiceProviderRepositoryContract;
use Plenty\Plugin\Log\Loggable;

/**
 * Class CreateShippingServiceProviderDPDShippingTutorial
 * @package DPDShippingTutorial\Migrations
 */
class CreateShippingServiceProviderSpringGDS
{
    use Loggable;

    /** @var ShippingServiceProviderRepositoryContract $shippingServiceProviderRepository */
    private $shippingServiceProviderRepository;

    /** @param ShippingServiceProviderRepositoryContract $shippingServiceProviderRepository */
    public function __construct(ShippingServiceProviderRepositoryContract $shippingServiceProviderRepository)
    {
        $this->shippingServiceProviderRepository = $shippingServiceProviderRepository;
    }

    /** @return void */
    public function run(): void
    {
        try {
            $this->shippingServiceProviderRepository->saveShippingServiceProvider(
                'DPDShippingTutorial',
                'DPDShippingTutorial');
        } catch (\Exception $e) {
            $this->getLogger('DPDShippingTutorial')
                ->critical('Could not save or update shipping service provider DPDShippingTutorial');
        }
    }

}
